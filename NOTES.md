# Protocols

- Maidsafe Crust P2P: https://github.com/maidsafe/crust/blob/master/docs/connect.md
- QUIC implementation in Rust: https://github.com/djc/quinn
- Cloudflare's QUIC implementation: https://github.com/cloudflare/quiche


# Similar Projects to examine

other projects with similar goals may supply inspiration.

## Syncthing

[Syncthing](https://syncthing.net/) does inter-machine directory sync. it is written in go.

documentation is provided for the [specifications of its protocols](https://docs.syncthing.net/specs/). these protocols are
* a block protocol for data transfer
* a global discovery protocol for finding nodes
* as well as a local discovery protocol
* there is also a relay protocol (to allow communcation between nodes that are both behind NATs)