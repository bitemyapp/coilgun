.PHONY: build build-release test build-image bash-image docker-build docker-build-release docker-test

.DEFAULT_GOAL = help

## Build the project
build:
	cargo build

## Release build
build-release:
	cargo build --release

## Run the rests
test:
	cargo test

## Run listen service
run-listen:
	cargo run --release -- listen --bind-port 9000

## Run send service
run-send:
	cargo run --release -- send --bind-port 9001 --dest-port 9000

## Build the Docker image
build-image:
	docker build -t coilgun .

## Bash into the image
bash-image:
	docker run -v `pwd`:/coilgun -it coilgun /bin/bash

## Build the app inside Docker
docker-build:
	docker run -v `pwd`:/coilgun -it coilgun make build

## Release build insider Docker
docker-build-release:
	docker run -v `pwd`:/coilgun -it coilgun make build-release

## Run the tests inside Docker
docker-test:
	docker run -v `pwd`:/coilgun -it coilgun make test

# To get the full repo reference
# docker inspect --format='{{index .RepoDigests 0}}' $IMAGE_HASH

help:
	@echo "Please use \`make <target>' where <target> is one of\n\n"
	@awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "%-30s %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)
