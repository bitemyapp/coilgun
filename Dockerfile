FROM rust:1.54.0

RUN apt-get update \
 && apt-get -y install build-essential capnproto \
 && apt-get clean  \
 && rm -rf /var/lib/apt/lists/*

RUN mkdir /coilgun

VOLUME ["/coilgun"]

WORKDIR /coilgun
