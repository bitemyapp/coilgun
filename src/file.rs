use anyhow::Result;
use bytes::BytesMut;
use std::net::SocketAddr;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

pub async fn send_file(mut f: tokio::fs::File) -> Result<()> {
    let mut buffer = BytesMut::new();

    let addr = "127.0.0.1:8899";
    let listener = tokio::net::TcpListener::bind(addr).await?;
    let (mut stream, _socket_addr) = listener.accept().await?;
    loop {
        let result = f.read_buf(&mut buffer).await?;
        log::debug!("Read {} bytes from file into buffer", result);
        let result = stream.write_buf(&mut buffer).await?;
        if result > 0 {
            log::debug!("Wrote {} bytes", result);
        } else {
            log::debug!("Wrote zero bytes, halting.");
            break;
        }
    }
    Ok(())
}

pub async fn receive_file(socket_addr: SocketAddr, filename: &str) -> Result<()> {
    let socket = tokio::net::TcpSocket::new_v4()?;
    let mut stream = socket.connect(socket_addr).await?;
    let mut buffer = BytesMut::new();
    loop {
        let result = stream.read_buf(&mut buffer).await?;
        if result > 0 {
            log::debug!("Received {} bytes", result);
        } else {
            log::debug!("Received zero bytes, halting.");
            break;
        }
    }

    log::debug!("buffer was: {:?}", buffer);
    let mut f = tokio::fs::File::create(filename).await?;
    loop {
        let result = f.write_buf(&mut buffer).await?;
        if result > 0 {
            log::debug!("Wrote {} bytes to file {}", result, filename);
        } else {
            log::debug!("Wrote zero bytes, halting.");
            break;
        }
    }

    Ok(())
}
