use self::ping_message::PingMessage;
use self::pong_message::PongMessage;
use self::timestamp::Timestamp;
use crate::coilgun_capnp::root_message as capnp_root_message;
use crate::coilgun_capnp::root_message::msg as capnp_msg_type;
use tokio::time;

pub mod codec;
pub mod ping_message;
pub mod pong_message;
pub mod timestamp;

pub trait CapnpWrite<'a> {
    type Builder;

    fn write_capnp(&self, builder: &mut Self::Builder);
}

pub trait CapnpRead<'a>
where
    Self: Sized,
{
    type Reader;

    fn read_capnp(reader: Self::Reader) -> Result<Self>;
}

#[derive(PartialEq, Eq, Debug, Clone)]
pub struct RootMessage {
    pub timestamp: Timestamp,
    pub message_type: MessageType,
}

impl RootMessage {
    pub fn new_ping(timestamp: Timestamp, ping_message: PingMessage) -> RootMessage {
        RootMessage {
            timestamp,
            message_type: MessageType::Ping(ping_message),
        }
    }

    #[allow(dead_code)]
    pub fn new_pong(timestamp: Timestamp, pong_message: PongMessage) -> RootMessage {
        RootMessage {
            timestamp,
            message_type: MessageType::Pong(pong_message),
        }
    }
}

impl<'a> CapnpRead<'a> for RootMessage {
    type Reader = capnp_root_message::Reader<'a>;

    fn read_capnp(reader: Self::Reader) -> Result<Self> {
        let read_timestamp = reader.get_timestamp()?;
        let timestamp = Timestamp::read_capnp(read_timestamp)?;

        let read_message_type = reader.get_msg();
        let message_type = MessageType::read_capnp(read_message_type)?;

        Ok(RootMessage {
            timestamp,
            message_type,
        })
    }
}

impl<'a> CapnpWrite<'a> for RootMessage {
    type Builder = capnp_root_message::Builder<'a>;

    fn write_capnp(&self, builder: &mut Self::Builder) {
        {
            let mut timestamp = builder.reborrow().init_timestamp();
            self.timestamp.write_capnp(&mut timestamp);
        }
        {
            let mut msg_type_builder = builder.reborrow().init_msg();
            self.message_type.write_capnp(&mut msg_type_builder);
        }
    }
}

#[derive(PartialEq, Eq, Debug, Clone)]
pub enum MessageType {
    Ping(PingMessage),
    Pong(PongMessage),
}

impl<'a> CapnpRead<'a> for MessageType {
    type Reader = capnp_msg_type::Reader<'a>;

    fn read_capnp(reader: Self::Reader) -> Result<Self> {
        let which_reader = reader.which().expect("msg had unknown type!");
        match which_reader {
            capnp_msg_type::Which::Ping(ping_result) => {
                let ping_reader =
                    ping_result.expect("could not extract ping message from ping message type");
                Ok(MessageType::Ping(PingMessage::read_capnp(ping_reader)?))
            }
            capnp_msg_type::Which::Pong(pong_result) => {
                let pong_reader =
                    pong_result.expect("could not extract pong message from pong message type");
                Ok(MessageType::Pong(PongMessage::read_capnp(pong_reader)?))
            }
        }
    }
}

impl<'a> CapnpWrite<'a> for MessageType {
    type Builder = capnp_msg_type::Builder<'a>;

    fn write_capnp(&self, builder: &mut Self::Builder) {
        match self {
            MessageType::Ping(ping_message) => {
                let mut ping_message_builder = builder.reborrow().init_ping();
                ping_message.write_capnp(&mut ping_message_builder);
            }
            MessageType::Pong(pong_message) => {
                let mut pong_message_builder = builder.reborrow().init_pong();
                pong_message.write_capnp(&mut pong_message_builder);
            }
        }
    }
}

#[derive(Debug)]
pub enum Error {
    Capnp(capnp::Error),
    IO(std::io::Error),
    TokioTimer(time::error::Error),
}

impl From<capnp::Error> for Error {
    fn from(err: capnp::Error) -> Error {
        Error::Capnp(err)
    }
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Error {
        Error::IO(err)
    }
}

impl From<time::error::Error> for Error {
    fn from(err: time::error::Error) -> Error {
        Error::TokioTimer(err)
    }
}

type Result<T> = std::result::Result<T, Error>;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_root_ping_msg_iso() {
        let now_timestamp = Timestamp::now();

        let ping_message = PingMessage {
            msg: "blah".to_string(),
            filename: "yeet.txt".to_string(),
            file_size: 4,
            file_port: 8899,
        };

        let root_message = RootMessage::new_ping(now_timestamp, ping_message);

        let mut msg = ::capnp::message::Builder::new_default();
        {
            let mut root_ping_msg = msg.init_root::<capnp_root_message::Builder>();
            root_message.write_capnp(&mut root_ping_msg);
        }

        let msg_reader = msg.into_reader();
        let ping_reader = msg_reader
            .get_root::<capnp_root_message::Reader>()
            .expect("could not read root of message");

        let read_ping = RootMessage::read_capnp(ping_reader).unwrap();

        assert_eq!(root_message, read_ping);
    }

    #[test]
    fn test_root_pong_msg_iso() {
        let now_timestamp = Timestamp::now();
        let timestamp_earlier = Timestamp {
            seconds: now_timestamp.seconds - 5,
            nanos: now_timestamp.nanos,
        };
        let pong_message = PongMessage {
            msg: "blah blah blah".to_string(),
            ping_time: timestamp_earlier,
            ping_msg: "blah".to_string(),
        };
        let root_message = RootMessage::new_pong(now_timestamp, pong_message);

        let mut msg = ::capnp::message::Builder::new_default();
        {
            let mut root_pong_msg = msg.init_root::<capnp_root_message::Builder>();
            root_message.write_capnp(&mut root_pong_msg);
        }

        let msg_reader = msg.into_reader();
        let pong_reader = msg_reader
            .get_root::<capnp_root_message::Reader>()
            .expect("could not read root of message");

        let read_pong = RootMessage::read_capnp(pong_reader).unwrap();

        assert_eq!(root_message, read_pong);
    }
}

#[macro_export]
macro_rules! write_packed_message {
    ($value:expr, $buf:expr) => {{
        let mut message = ::capnp::message::Builder::new_default();
        {
            let inner = message.init_root();
            ::protocol::CapnP::build($value, inner);
        }
        ::capnp::serialize_packed::write_message(&mut $buf, &message)
    }};
}
