use super::{CapnpRead, CapnpWrite, Error, RootMessage};
use crate::coilgun_capnp::root_message as capnp_root_message;
// use bytes::buf::ext::BufExt;
// use bytes::buf::ext::BufMutExt;
use bytes::{Buf, BytesMut};
use capnp::message::ReaderOptions as CapnpReaderOptions;
use capnp::serialize as capnp_serialize;
use tokio_util::codec::{Decoder, Encoder};

pub struct CapnpMessageCodec {}

impl CapnpMessageCodec {
    pub fn new() -> CapnpMessageCodec {
        CapnpMessageCodec {}
    }
}

impl Encoder<RootMessage> for CapnpMessageCodec {
    // type Item = RootMessage;
    type Error = Error;

    fn encode(&mut self, item: RootMessage, dst: &mut BytesMut) -> Result<(), Self::Error> {
        use bytes::BufMut;
        let mut msg = ::capnp::message::Builder::new_default();
        {
            let mut m_builder = msg.init_root::<capnp_root_message::Builder>();
            item.write_capnp(&mut m_builder);
        }

        let word_count = capnp_serialize::compute_serialized_size_in_words(&msg);
        // capnp rust crate documentation says that their Word is 8 bytes - and currently uses a u64
        let byte_count = word_count * 8;

        // ugly, yeah
        dst.reserve(byte_count);

        let mut dst_writer = dst.writer();

        // yes, write_message returns std::io::Result<()>
        Ok(capnp_serialize::write_message(&mut dst_writer, &msg)?)
    }
}

impl Decoder for CapnpMessageCodec {
    type Item = RootMessage;
    type Error = Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        // use bytes::buf::into_buf::IntoBuf;
        // if src.len() > 0 {
        if !src.is_empty() {
            let len = src.len();
            // all of these operations should be constant-time, according to the bytes docs
            let split_src = src.split_to(len);
            let mut buf_reader = split_src.reader();
            let read_value =
                capnp_serialize::read_message(&mut buf_reader, CapnpReaderOptions::new())?;

            let root_message_reader = read_value.get_root::<capnp_root_message::Reader>()?;

            let converted = RootMessage::read_capnp(root_message_reader)?;

            Ok(Some(converted))
        } else {
            Ok(None)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::messages::ping_message::PingMessage;
    use crate::messages::pong_message::PongMessage;
    use crate::messages::timestamp::Timestamp;

    #[test]
    fn test_root_ping_msg_codec_iso() {
        let now_timestamp = Timestamp::now();

        let ping_message = PingMessage {
            msg: "blah".to_string(),
            filename: "yeet.txt".to_string(),
            file_size: 4,
            file_port: 8899,
        };

        let root_message = RootMessage::new_ping(now_timestamp, ping_message);

        let mut codec = CapnpMessageCodec::new();
        let mut msg_buffer = BytesMut::with_capacity(1);
        codec
            .encode(root_message.clone(), &mut msg_buffer)
            .expect("failed to encode message");

        let mut copy_buffer = msg_buffer.clone();

        let result_message = codec
            .decode(&mut copy_buffer)
            .expect("failed to decode message")
            .expect("no message produced by decode");

        assert_eq!(root_message, result_message);
    }

    #[test]
    fn test_root_pong_msg_codec_iso() {
        let now_timestamp = Timestamp::now();
        let timestamp_earlier = Timestamp {
            seconds: now_timestamp.seconds - 5,
            nanos: now_timestamp.nanos,
        };
        let pong_message = PongMessage {
            msg: "blah blah blah".to_string(),
            ping_time: timestamp_earlier,
            ping_msg: "blah".to_string(),
        };

        let root_message = RootMessage::new_pong(now_timestamp, pong_message);

        let mut codec = CapnpMessageCodec::new();
        let mut msg_buffer = BytesMut::with_capacity(1);
        codec
            .encode(root_message.clone(), &mut msg_buffer)
            .expect("failed to encode message");

        let mut copy_buffer = msg_buffer.clone();

        let result_message = codec
            .decode(&mut copy_buffer)
            .expect("failed to decode message")
            .expect("no message produced by decode");

        assert_eq!(root_message, result_message);
    }
}
