use super::timestamp::Timestamp;
use super::{CapnpRead, CapnpWrite, Result};
use crate::coilgun_capnp::pong_message;

#[derive(PartialEq, Eq, Debug, Clone)]
pub struct PongMessage {
    pub msg: String,
    pub ping_time: Timestamp,
    pub ping_msg: String,
}

impl<'a> CapnpWrite<'a> for PongMessage {
    type Builder = pong_message::Builder<'a>;

    fn write_capnp(&self, builder: &mut Self::Builder) {
        builder.set_msg(&self.msg);
        {
            let mut ping_time = builder.reborrow().init_ping_time();
            self.ping_time.write_capnp(&mut ping_time);
        }
        builder.set_ping_msg(&self.ping_msg);
    }
}

impl<'a> CapnpRead<'a> for PongMessage {
    type Reader = pong_message::Reader<'a>;

    fn read_capnp(reader: Self::Reader) -> Result<Self> {
        let msg = reader.get_msg()?;

        let read_ping_time = reader.get_ping_time()?;
        let ping_time = Timestamp::read_capnp(read_ping_time)?;

        let ping_msg = reader.get_ping_msg()?;

        Ok(PongMessage {
            msg: msg.to_string(),
            ping_time,
            ping_msg: ping_msg.to_string(),
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_pong_message_iso() {
        let timestamp = Timestamp::now();

        let timestamp_earlier = Timestamp {
            seconds: timestamp.seconds - 5,
            nanos: timestamp.nanos,
        };

        let pong_message = PongMessage {
            msg: "blah blah blah".to_string(),
            ping_time: timestamp_earlier,
            ping_msg: "blah!!!".to_string(),
        };

        let mut msg = ::capnp::message::Builder::new_default();
        {
            let mut pong_msg = msg.init_root::<pong_message::Builder>();
            pong_message.write_capnp(&mut pong_msg);
        }

        let msg_reader = msg.into_reader();
        let pong_reader = msg_reader
            .get_root::<pong_message::Reader>()
            .expect("could not read root of message as pong message");

        let read_pong = PongMessage::read_capnp(pong_reader).unwrap();

        assert_eq!(pong_message, read_pong);
    }
}
