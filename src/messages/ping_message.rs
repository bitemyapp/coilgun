use super::{CapnpRead, CapnpWrite, Result};
use crate::coilgun_capnp::ping_message;

#[derive(PartialEq, Eq, Debug, Clone)]
pub struct PingMessage {
    pub msg: String,
    pub filename: String,
    pub file_size: u64,
    pub file_port: u64,
}

impl<'a> CapnpWrite<'a> for PingMessage {
    type Builder = ping_message::Builder<'a>;

    fn write_capnp(&self, builder: &mut Self::Builder) {
        builder.set_msg(&self.msg);
        builder.set_filename(&self.filename);
        builder.set_file_size(self.file_size);
        builder.set_file_port(self.file_port);
    }
}

impl<'a> CapnpRead<'a> for PingMessage {
    type Reader = ping_message::Reader<'a>;

    fn read_capnp(reader: Self::Reader) -> Result<Self> {
        let msg = reader.get_msg()?;
        let filename = reader.get_filename()?;
        let file_size = reader.get_file_size();
        let file_port = reader.get_file_port();
        Ok(PingMessage {
            msg: msg.to_string(),
            filename: filename.to_string(),
            file_size: file_size,
            file_port: file_port,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_ping_message_iso() {
        let ping_message = PingMessage {
            msg: "blah".to_string(),
            filename: "yeet.txt".to_string(),
            file_size: 4,
            file_port: 8899,
        };

        let mut msg = ::capnp::message::Builder::new_default();
        {
            let mut ping_msg = msg.init_root::<ping_message::Builder>();
            ping_message.write_capnp(&mut ping_msg);
        }

        let msg_reader = msg.into_reader();
        let ping_reader = msg_reader
            .get_root::<ping_message::Reader>()
            .expect("could not read root of message as ping messag4e");

        let read_ping = PingMessage::read_capnp(ping_reader).unwrap();

        assert_eq!(ping_message, read_ping);
    }
}
