use crate::coilgun_capnp::timestamp;

use super::{CapnpRead, CapnpWrite, Result};
use std::time::SystemTime;

#[derive(PartialEq, Eq, Debug, Clone)]
pub struct Timestamp {
    pub seconds: u64,
    pub nanos: u32,
}

impl Timestamp {
    pub fn now() -> Timestamp {
        let duration = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .expect("unix epoch should be earlier than now, check system clock.");

        Timestamp {
            seconds: duration.as_secs(),
            nanos: duration.subsec_nanos(),
        }
    }
}

impl<'a> CapnpWrite<'a> for Timestamp {
    type Builder = timestamp::Builder<'a>;

    fn write_capnp(&self, builder: &mut Self::Builder) {
        builder.set_seconds(self.seconds);
        builder.set_nanos(self.nanos);
    }
}

impl<'a> CapnpRead<'a> for Timestamp {
    type Reader = timestamp::Reader<'a>;

    fn read_capnp(reader: Self::Reader) -> Result<Self> {
        let seconds = reader.get_seconds();
        let nanos = reader.get_nanos();
        Ok(Timestamp { seconds, nanos })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_timestamp_capnp_iso() {
        let timestamp = Timestamp::now();
        let mut msg = ::capnp::message::Builder::new_default();
        {
            let mut timestamp_msg = msg.init_root::<timestamp::Builder>();
            timestamp.write_capnp(&mut timestamp_msg);
        }

        let msg_reader = msg.into_reader();
        let timestamp_reader = msg_reader
            .get_root::<timestamp::Reader>()
            .expect("could not read root of message as timestamp");

        let read_timestamp = Timestamp::read_capnp(timestamp_reader).unwrap();

        assert_eq!(timestamp, read_timestamp)
    }
}
