use anyhow::Result;
use libp2p::mdns::service::{MdnsPacket, MdnsService};

#[tokio::main]
async fn main() -> Result<()> {
    // This example provides passive discovery of the libp2p nodes on the
    // network that send mDNS queries and answers.

}
