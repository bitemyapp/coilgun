use anyhow::Result;
use libp2p::mdns::service::{
    build_query_response, build_service_discovery_response, MdnsPacket, MdnsService,
};
use std::time::Duration;

#[tokio::main]
async fn main() -> Result<()> {
    // This example provides passive discovery of the libp2p nodes on the
    // network that send mDNS queries and answers.
    let mut service = MdnsService::new().await?;
    let my_peer_id = libp2p::PeerId::random();
    loop {
        let (mut srv, packet) = service.next().await;
        match packet {
            MdnsPacket::Query(query) => {
                log::debug!("Query from {:?}", query.remote_addr());
                let packets = build_query_response(
                    query.query_id(),
                    my_peer_id.clone(),
                    vec![].into_iter(),
                    Duration::from_secs(120),
                );
                for packet in packets {
                    srv.enqueue_response(packet);
                }
            }
            MdnsPacket::Response(response) => {
                log::debug!("remote_addr: {:?}", response.remote_addr());
                for peer in response.discovered_peers() {
                    log::debug!("Discovered peer {:?}", peer.id());
                    log::debug!("addresses: {:?}", peer.addresses());
                    for addr in peer.addresses() {
                        log::debug!("Address = {:?}", addr);
                    }
                }
            }
            MdnsPacket::ServiceDiscovery(disc) => {
                let resp =
                    build_service_discovery_response(disc.query_id(), Duration::from_secs(120));
                srv.enqueue_response(resp);
            }
        }
        service = srv
    }
}
