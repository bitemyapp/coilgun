use self::messages::codec::CapnpMessageCodec;
use self::messages::ping_message::PingMessage;
// use self::messages::pong_message::PongMessage;
use self::messages::timestamp::Timestamp;
use self::messages::{MessageType, RootMessage};

use anyhow::Result;
use futures::prelude::*;
use std::net::IpAddr;
use std::net::Ipv4Addr;
use std::net::SocketAddr;
use std::path::Path;
use std::time::Duration;
use structopt::StructOpt;
use tokio::net::UdpSocket;
use tokio::time::interval;
use tokio_util::udp::UdpFramed;

mod file;
mod messages;

pub mod coilgun_capnp {
    include!(concat!(env!("OUT_DIR"), "/coilgun_capnp.rs"));
}

#[derive(StructOpt, Debug)]
#[structopt(name = "coilgun")]
/// Demo - send or receive heartbeats
struct CoilgunOpt {
    #[structopt(subcommand)]
    command: CoilgunCommand,
}

#[derive(StructOpt, Debug)]
enum CoilgunCommand {
    #[structopt(name = "send")]
    /// sends heartbeat signal
    Send(SendParams),

    #[structopt(name = "listen")]
    /// listens for heartbeat signal
    Listen(ListenParams),
}

#[derive(StructOpt, Debug)]
struct SendParams {
    #[structopt(long = "delay", default_value = "1", value_name = "SECONDS")]
    /// how long to wait between heartbeats
    delay: u16,

    #[structopt(short = "b", long = "bind-port", value_name = "PORT", default_value = "8051")]
    /// port to send heartbeats from and listen for responses on
    bind_port: u16,

    #[structopt(short = "d", long = "dest-port", value_name = "PORT", default_value = "8050")]
    /// port to send hearbeats to
    dest_port: u16,

    #[structopt(short = "m", long = "message", value_name = "MESSAGE")]
    /// Message to pass along to the user you're offering the file
    message: String,

    #[structopt(short = "f", long = "filename", value_name = "FILENAME")]
    /// File to send via Coilgun
    filename: String,
}

#[derive(StructOpt, Debug)]
struct ListenParams {
    #[structopt(long = "delay", default_value = "0", value_name = "SECONDS")]
    /// how long to wait before responding to a heartbeat
    delay: u16,

    #[structopt(short = "b", long = "bind-port", value_name = "PORT", default_value = "8050")]
    /// Port to listen for heartbeats on
    bind_port: u16,

    #[structopt(short = "s", long = "max-size", value_name = "MAX_SIZE", default_value = "20MB")]
    /// Maximum number of bytes to accept
    max_bytes: String,
}

#[tokio::main]
// async fn main() -> Result<(), std::io::Error> {
async fn main() -> Result<()> {
    env_logger::init();

    let opts = CoilgunOpt::from_args();

    match opts.command {
        CoilgunCommand::Send(params) => perform_send(params).await?,
        CoilgunCommand::Listen(params) => perform_listen(params).await?,
    };
    // match opts.command {
    //     CoilgunCommand::Send(params) => file::send_file("shared/sheena.txt").await?,
    //     CoilgunCommand::Listen(params) => file::receive_file("new_sheena.txt").await?,
    // };
    Ok(())
}

async fn perform_send(params: SendParams) -> Result<()> {
    log::debug!("perform_send(params: {:?})", params);
    let path = Path::new(&params.filename);
    let filename_only = path
        .file_name()
        .expect("Path didn't parse")
        .to_str()
        .expect("OsStr wasn't a valid string")
        .to_string();
    let f = tokio::fs::File::open(params.filename.clone()).await?;
    let file_metadata = f.metadata().await?;
    let file_length = file_metadata.len();
    log::debug!("{:?}, {:?}", filename_only, file_length);
    let localhost = IpAddr::V4(Ipv4Addr::LOCALHOST);
    let address = SocketAddr::new(localhost, params.bind_port);

    let socket = UdpSocket::bind(&address).await?;
    let framed_socket: UdpFramed<CapnpMessageCodec> =
        UdpFramed::new(socket, CapnpMessageCodec::new());

    let (mut sink, stream) = framed_socket.split();

    let dest_addr = SocketAddr::new(localhost, params.dest_port);

    tokio::spawn(async move { file::send_file(f).await });

    tokio::spawn(async move {
        let mut interval_stream =
            // casting u16 to u64 may become silently lossy if types change
            // Interval::new(Instant::now(), Duration::from_secs(params.delay as u64))
            tokio_stream::wrappers::IntervalStream::new(interval(Duration::from_secs(u64::from(params.delay))))
                .map(move |_i| {
                    let ping_msg = PingMessage {
                        msg: params.message.clone(),
                        filename: filename_only.clone(),
                        file_size: file_length,
                        file_port: 8899,
                    };
                    let msg = RootMessage::new_ping(Timestamp::now(), ping_msg);
                    log::debug!("msg: {:?}", msg);
                    Ok((msg, dest_addr))
                });
        sink.send_all(&mut interval_stream).await
    });

    stream
        .for_each(|response| match response {
            Ok((msg, addr)) => {
                log::debug!("received {:?} from {:?}", msg, addr);
                future::ready(())
            }
            Err(error) => {
                log::error!("received error: {:?}", error);
                future::ready(())
            }
        })
        .await;

    Ok(())
}

fn prompt_user(prompt: &str) -> Result<String, std::io::Error> {
    use std::io::Read;
    use std::io::Write;
    println!("{}", prompt);
    let _ = std::io::stdout().flush();
    let mut input = String::new();
    std::io::stdin().read_line(&mut input)?;
    Ok(input)
}

async fn perform_listen(params: ListenParams) -> Result<()> {
    log::debug!("perform_listen(params: {:?})", params);
    let localhost = IpAddr::V4(Ipv4Addr::LOCALHOST);
    let address = SocketAddr::new(localhost, params.bind_port);
    let socket = UdpSocket::bind(&address).await?;
    let (_sink, mut stream) = UdpFramed::new(socket, CapnpMessageCodec::new()).split();

    loop {
        let (msg, addr) = stream
            .next()
            .await
            .expect("None stream next")
            .expect("messages error");
        let ip_address = addr.ip();
        match msg.message_type {
            MessageType::Ping(ping) => {
                let prompt = format!("Someone is trying to send you a file. The offer includes the message: {}\nThe filename is: {}\nDo you accept the message? If so, type in the destination filename and hit either, otherwise, hit enter with no input to cancel", ping.msg, ping.filename);
                let response = prompt_user(&prompt)?;
                let filename = match response.as_str() {
                    "" => {
                        println!("Exiting because you entered no input, no files downloaded");
                        std::process::exit(0);
                    },
                    filename => filename.trim_end(),
                };
                log::debug!("Filename was: {:?}", filename);
                let socket_addr = SocketAddr::new(ip_address, ping.file_port as u16);
                file::receive_file(socket_addr, &filename).await?;
                log::info!("File {} received!", &ping.filename);
                break;
            }
            MessageType::Pong(_) => {
                unimplemented!()
            }
        }
    }
    Ok(())
}
