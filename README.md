coilgun
=======

`coilgun` is a simple, cross-platform'ish LAN filedrop utility.

It's built in rust, and it uses multicast.

You shouldn't actually use this, it's insecure and buggy. I finally open-sourced this because I saw some people on Reddit talking about making a file transfer utility in Rust.

## Compilation

You'll need to have `capnproto` installed to build the application. On Ubuntu this looks like:

```
sudo apt install capnproto
```

## Use

You really shouldn't use this, this was really a glorified prototype for discovering computers on a local arena network with mdns.

On the receiving side:

```
coilgun listen
```


On the sending side:

```
coilgun send --filename README.md --message "The data you asked for"
```

Once both sides have started, the listening side will see:

```
Someone is trying to send you a file. The offer includes the message: The data you asked for
The filename is: README.md
Do you accept the message? If so, type in the destination filename and hit either, otherwise, hit enter with no input to cancel
```

The listening side can then either hit enter to cancel or type in the filename they want to download the file to and hit enter to receive. You can also ctrl-c out. The sending side will need to out-of-band confirm receipt and hit ctrl-c.

## Making this more useful

- Encryption (Is more than TLS needed? If so, what and why?)
- Converting it into a library that a daemon could use to passively listen for file transfers
- Systray app
- Letting the sender see the heartbearts and choose a specific computer to transmit to
- Telling the sender when the file has sent and closing the app (the sender code is a little gnarly)
- Set size limits on all message types and enforce them. I'm pretty sure you could make a big filename and tie up the app for a bit.
