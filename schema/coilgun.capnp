@0x8e630b2f41d7ac81;

struct RootMessage {
    timestamp @0 :Timestamp;
    msg :union {
       ping @1 :PingMessage;
       pong @2 :PongMessage;
    }
}

struct PingMessage {
    msg       @0 :Text;
    filename  @1 :Text;
    fileSize  @2 :UInt64;
    filePort  @3 :UInt64;
}

struct PongMessage {
    msg       @0 :Text;
    pingTime @1 :Timestamp;
    pingMsg  @2 :Text;
}

struct Timestamp {
    seconds @0 :UInt64;
    nanos   @1 :UInt32;
}