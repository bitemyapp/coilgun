extern crate capnpc;

use capnpc::CompilerCommand;

fn main() {
    CompilerCommand::new()
        .src_prefix("schema")
        .file("schema/coilgun.capnp")
        .run()
        .expect("captain proto schema generation failed");
}
